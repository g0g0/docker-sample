# Sample Sinatra Docker Application

## Requirements

1. [Docker](https://docs.docker.com/install/)
2. [Docker Compose](https://docs.docker.com/compose/install/)

## Initial Build

```bash
docker-compose build
```

> re-build

```bash
docker-compose up --build
```

## Running

> Not required if using docker-compose up

```bash
docker-compose start
```

> stopping

```bash
docker-compose stop
```

## RSpec

```bash
docker-compose exec -e APP_ENV=test web rspec
```

## Cleanup

```bash
docker-compose down
```

> remove persistent volumes (lose all data)

```bash
docker-compose down -v
```
