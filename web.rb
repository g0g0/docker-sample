require 'rubygems'
require 'bundler/setup'
Bundler.require(:default, ENV['APP_ENV'])

require_relative 'config/config'

set :bind, '0.0.0.0'

get '/' do
  'Hello world!'
end
