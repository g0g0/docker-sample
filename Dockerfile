FROM ruby:2.5.1
RUN apt-get update

RUN mkdir /app
WORKDIR /app
COPY . /app

RUN bundle install
